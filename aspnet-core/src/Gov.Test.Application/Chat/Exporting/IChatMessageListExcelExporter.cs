﻿using System.Collections.Generic;
using Abp;
using Gov.Test.Chat.Dto;
using Gov.Test.Dto;

namespace Gov.Test.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(UserIdentifier user, List<ChatMessageExportDto> messages);
    }
}
