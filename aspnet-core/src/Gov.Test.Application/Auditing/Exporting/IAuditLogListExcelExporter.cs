﻿using System.Collections.Generic;
using Gov.Test.Auditing.Dto;
using Gov.Test.Dto;

namespace Gov.Test.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
