﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Gov.Test.MultiTenancy.Accounting.Dto;

namespace Gov.Test.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
