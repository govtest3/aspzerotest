﻿using System.Collections.Generic;
using Gov.Test.Authorization.Users.Importing.Dto;
using Abp.Dependency;

namespace Gov.Test.Authorization.Users.Importing
{
    public interface IUserListExcelDataReader: ITransientDependency
    {
        List<ImportUserDto> GetUsersFromExcel(byte[] fileBytes);
    }
}
