﻿using System.Collections.Generic;
using Gov.Test.Authorization.Users.Importing.Dto;
using Gov.Test.Dto;

namespace Gov.Test.Authorization.Users.Importing
{
    public interface IInvalidUserExporter
    {
        FileDto ExportToFile(List<ImportUserDto> userListDtos);
    }
}
