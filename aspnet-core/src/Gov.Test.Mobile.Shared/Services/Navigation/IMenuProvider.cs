﻿using System.Collections.Generic;
using MvvmHelpers;
using Gov.Test.Models.NavigationMenu;

namespace Gov.Test.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}