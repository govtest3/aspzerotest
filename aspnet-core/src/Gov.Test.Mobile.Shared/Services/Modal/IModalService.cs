﻿using System.Threading.Tasks;
using Gov.Test.Views;
using Xamarin.Forms;

namespace Gov.Test.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}
