﻿namespace Gov.Test.Services.Permission
{
    public interface IPermissionService
    {
        bool HasPermission(string key);
    }
}