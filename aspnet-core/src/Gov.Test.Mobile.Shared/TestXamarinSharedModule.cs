﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Gov.Test
{
    [DependsOn(typeof(TestClientModule), typeof(AbpAutoMapperModule))]
    public class TestXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TestXamarinSharedModule).GetAssembly());
        }
    }
}