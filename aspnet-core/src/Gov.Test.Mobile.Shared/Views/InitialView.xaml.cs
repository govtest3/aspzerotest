﻿using Xamarin.Forms;

namespace Gov.Test.Views
{
    public partial class InitialView : ContentPage, IXamarinView
    {
        public InitialView()
        {
            InitializeComponent();
        }
    }
}