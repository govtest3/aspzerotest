﻿using Xamarin.Forms.Internals;

namespace Gov.Test.Behaviors
{
    [Preserve(AllMembers = true)]
    public interface IAction
    {
        bool Execute(object sender, object parameter);
    }
}