﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Gov.Test.Web.Public.Views
{
    public abstract class TestViewComponent : AbpViewComponent
    {
        protected TestViewComponent()
        {
            LocalizationSourceName = TestConsts.LocalizationSourceName;
        }
    }
}