﻿using Abp.Dependency;
using GraphQL.Types;
using GraphQL.Utilities;
using Gov.Test.Queries.Container;
using System;

namespace Gov.Test.Schemas
{
    public class MainSchema : Schema, ITransientDependency
    {
        public MainSchema(IServiceProvider provider) :
            base(provider)
        {
            Query = provider.GetRequiredService<QueryContainer>();
        }
    }
}