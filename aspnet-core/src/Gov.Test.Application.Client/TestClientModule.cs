﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Gov.Test
{
    public class TestClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TestClientModule).GetAssembly());
        }
    }
}
