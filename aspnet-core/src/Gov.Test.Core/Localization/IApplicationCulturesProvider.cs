﻿using System.Globalization;

namespace Gov.Test.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}