﻿using Microsoft.Extensions.Configuration;

namespace Gov.Test.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
