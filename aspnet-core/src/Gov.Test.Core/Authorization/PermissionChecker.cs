﻿using Abp.Authorization;
using Gov.Test.Authorization.Roles;
using Gov.Test.Authorization.Users;

namespace Gov.Test.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
