﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Gov.Test
{
    public class TestCoreSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TestCoreSharedModule).GetAssembly());
        }
    }
}