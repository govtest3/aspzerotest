﻿using System.Collections.Generic;

namespace Gov.Test.Authorization.Roles.Dto
{
    public class GetRolesInput
    {
        public List<string> Permissions { get; set; }
    }
}
