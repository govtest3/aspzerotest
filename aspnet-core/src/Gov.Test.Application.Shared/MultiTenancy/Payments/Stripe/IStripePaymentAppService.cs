﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Gov.Test.MultiTenancy.Payments.Dto;
using Gov.Test.MultiTenancy.Payments.Stripe.Dto;

namespace Gov.Test.MultiTenancy.Payments.Stripe
{
    public interface IStripePaymentAppService : IApplicationService
    {
        Task ConfirmPayment(StripeConfirmPaymentInput input);

        StripeConfigurationDto GetConfiguration();

        Task<SubscriptionPaymentDto> GetPaymentAsync(StripeGetPaymentInput input);

        Task<string> CreatePaymentSession(StripeCreatePaymentSessionInput input);
    }
}