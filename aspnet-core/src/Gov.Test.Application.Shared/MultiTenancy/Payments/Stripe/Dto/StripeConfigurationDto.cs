﻿namespace Gov.Test.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeConfigurationDto
    {
        public string PublishableKey { get; set; }
    }
}
