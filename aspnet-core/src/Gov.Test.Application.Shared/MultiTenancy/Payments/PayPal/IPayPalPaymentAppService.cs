﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Gov.Test.MultiTenancy.Payments.PayPal.Dto;

namespace Gov.Test.MultiTenancy.Payments.PayPal
{
    public interface IPayPalPaymentAppService : IApplicationService
    {
        Task ConfirmPayment(long paymentId, string paypalOrderId);

        PayPalConfigurationDto GetConfiguration();
    }
}
