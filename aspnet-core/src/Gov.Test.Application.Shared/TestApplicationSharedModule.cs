﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Gov.Test
{
    [DependsOn(typeof(TestCoreSharedModule))]
    public class TestApplicationSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TestApplicationSharedModule).GetAssembly());
        }
    }
}