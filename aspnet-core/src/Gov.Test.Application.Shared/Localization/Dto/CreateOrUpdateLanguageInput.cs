﻿using System.ComponentModel.DataAnnotations;

namespace Gov.Test.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}