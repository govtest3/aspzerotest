﻿namespace Gov.Test.Sessions.Dto
{
    public class SubscriptionPaymentInfoDto
    {
        public decimal Amount { get; set; }
    }
}