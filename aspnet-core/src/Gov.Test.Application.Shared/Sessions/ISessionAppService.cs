﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Gov.Test.Sessions.Dto;

namespace Gov.Test.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
