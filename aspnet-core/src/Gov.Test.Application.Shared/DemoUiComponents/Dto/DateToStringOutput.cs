﻿namespace Gov.Test.DemoUiComponents.Dto
{
    public class DateToStringOutput
    {
        public string DateString { get; set; }
    }
}
