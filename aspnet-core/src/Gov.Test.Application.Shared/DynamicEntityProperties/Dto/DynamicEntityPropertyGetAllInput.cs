﻿namespace Gov.Test.DynamicEntityProperties
{
    public class DynamicEntityPropertyGetAllInput
    {
        public string EntityFullName { get; set; }
    }
}
