﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Gov.Test.Configuration.Host.Dto;

namespace Gov.Test.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
