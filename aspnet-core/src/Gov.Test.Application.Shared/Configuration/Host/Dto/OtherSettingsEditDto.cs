﻿namespace Gov.Test.Configuration.Host.Dto
{
    public class OtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}