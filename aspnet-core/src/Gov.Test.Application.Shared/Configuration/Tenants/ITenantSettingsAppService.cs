﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Gov.Test.Configuration.Tenants.Dto;

namespace Gov.Test.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
