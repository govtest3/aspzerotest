﻿using Abp.Auditing;
using Gov.Test.Configuration.Dto;

namespace Gov.Test.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}