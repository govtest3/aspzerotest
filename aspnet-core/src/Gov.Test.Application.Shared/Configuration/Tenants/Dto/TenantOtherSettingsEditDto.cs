﻿namespace Gov.Test.Configuration.Tenants.Dto
{
    public class TenantOtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }
}