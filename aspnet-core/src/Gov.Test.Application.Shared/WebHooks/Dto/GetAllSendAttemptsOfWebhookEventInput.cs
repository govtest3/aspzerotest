﻿namespace Gov.Test.WebHooks.Dto
{
    public class GetAllSendAttemptsOfWebhookEventInput
    {
        public string Id { get; set; }
    }
}
