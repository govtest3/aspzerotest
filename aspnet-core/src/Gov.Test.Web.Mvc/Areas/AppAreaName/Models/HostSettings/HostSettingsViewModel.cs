﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Gov.Test.Configuration.Host.Dto;
using Gov.Test.Editions.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.HostSettings
{
    public class HostSettingsViewModel
    {
        public HostSettingsEditDto Settings { get; set; }

        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }

        public List<ComboboxItemDto> TimezoneItems { get; set; }

        public List<string> EnabledSocialLoginSettings { get; set; } = new List<string>();
    }
}