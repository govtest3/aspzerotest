﻿using Gov.Test.MultiTenancy.Accounting.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Accounting
{
    public class InvoiceViewModel
    {
        public InvoiceDto Invoice { get; set; }
    }
}
