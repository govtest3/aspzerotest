﻿using Abp.Application.Services.Dto;
using Abp.Webhooks;
using Gov.Test.WebHooks.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Webhooks
{
    public class CreateOrEditWebhookSubscriptionViewModel
    {
        public WebhookSubscription WebhookSubscription { get; set; }

        public ListResultDto<GetAllAvailableWebhooksOutput> AvailableWebhookEvents { get; set; }
    }
}
