﻿using System.Collections.Generic;
using Gov.Test.Caching.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Maintenance
{
    public class MaintenanceViewModel
    {
        public IReadOnlyList<CacheDto> Caches { get; set; }
    }
}