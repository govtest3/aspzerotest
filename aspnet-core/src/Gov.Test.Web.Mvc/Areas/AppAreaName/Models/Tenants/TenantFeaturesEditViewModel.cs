﻿using Abp.AutoMapper;
using Gov.Test.MultiTenancy;
using Gov.Test.MultiTenancy.Dto;
using Gov.Test.Web.Areas.AppAreaName.Models.Common;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Tenants
{
    [AutoMapFrom(typeof (GetTenantFeaturesEditOutput))]
    public class TenantFeaturesEditViewModel : GetTenantFeaturesEditOutput, IFeatureEditViewModel
    {
        public Tenant Tenant { get; set; }
    }
}