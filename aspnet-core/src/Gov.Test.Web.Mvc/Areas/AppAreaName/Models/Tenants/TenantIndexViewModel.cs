﻿using System.Collections.Generic;
using Gov.Test.Editions.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Tenants
{
    public class TenantIndexViewModel
    {
        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }
    }
}