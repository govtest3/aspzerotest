﻿using Gov.Test.Sessions.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Editions
{
    public class SubscriptionDashboardViewModel
    {
        public GetCurrentLoginInformationsOutput LoginInformations { get; set; }
    }
}
