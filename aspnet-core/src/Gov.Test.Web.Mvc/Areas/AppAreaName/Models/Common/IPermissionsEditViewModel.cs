﻿using System.Collections.Generic;
using Gov.Test.Authorization.Permissions.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}