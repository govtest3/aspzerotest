﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Gov.Test.Web.Areas.AppAreaName.Models.Layout;
using Gov.Test.Web.Session;
using Gov.Test.Web.Views;

namespace Gov.Test.Web.Areas.AppAreaName.Views.Shared.Themes.Theme8.Components.AppAreaNameTheme8Footer
{
    public class AppAreaNameTheme8FooterViewComponent : TestViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppAreaNameTheme8FooterViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var footerModel = new FooterViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync()
            };

            return View(footerModel);
        }
    }
}
