﻿using System.Collections.Generic;
using Gov.Test.Editions.Dto;
using Gov.Test.MultiTenancy.Payments;

namespace Gov.Test.Web.Models.Payment
{
    public class ExtendEditionViewModel
    {
        public EditionSelectDto Edition { get; set; }

        public List<PaymentGatewayModel> PaymentGateways { get; set; }
    }
}