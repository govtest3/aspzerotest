﻿using System.Collections.Generic;
using Abp.Localization;
using Gov.Test.Install.Dto;

namespace Gov.Test.Web.Models.Install
{
    public class InstallViewModel
    {
        public List<ApplicationLanguage> Languages { get; set; }

        public AppSettingsJsonDto AppSettingsJson { get; set; }
    }
}
