﻿using Gov.Test.Editions;
using Gov.Test.Editions.Dto;
using Gov.Test.MultiTenancy.Payments;
using Gov.Test.Security;
using Gov.Test.MultiTenancy.Payments.Dto;

namespace Gov.Test.Web.Models.TenantRegistration
{
    public class TenantRegisterViewModel
    {
        public PasswordComplexitySetting PasswordComplexitySetting { get; set; }

        public int? EditionId { get; set; }

        public SubscriptionStartType? SubscriptionStartType { get; set; }

        public EditionSelectDto Edition { get; set; }

        public EditionPaymentType EditionPaymentType { get; set; }
    }
}
