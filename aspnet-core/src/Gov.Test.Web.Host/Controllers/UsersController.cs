﻿using Abp.AspNetCore.Mvc.Authorization;
using Gov.Test.Authorization;
using Gov.Test.Storage;
using Abp.BackgroundJobs;

namespace Gov.Test.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Users)]
    public class UsersController : UsersControllerBase
    {
        public UsersController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
            : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}